export default function ({ $axios, store }) {
    $axios.onRequest(config => {
        console.log(config.headers.common)
        if (store.state.wp_nonce.credential !== ''){
            config.headers.common['X-WP-Nonce'] = store.state.wp_nonce.credential
        }
    })
}