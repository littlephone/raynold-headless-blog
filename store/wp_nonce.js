export const state = () => ({
    credential: ''
})

export const mutations = {
    setWPNonce(state, token) {
       state.credential = token
    },

    resetWPNonce(state) {
        state.credential = ''
    },
    getWPNonce() {
        return state.credential;
    }
}